/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model_PSO;

/**
 *
 * @author longy
 */
public class Sensor {
    public Point point;
    static double r;

    public Sensor(Point point, double r) {
        this.point = point;
        this.r = r;
    }
    public Sensor(double x,double y,double r){
        this.point = new Point(x,y);
        this.r = r;
    }
    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
}
