/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model_PSO;

/**
 *
 * @author longy
 */
public class Pbest {
    public static final int NUMPOINT = 5001;
    public Point[] point ;
    public double Objective;

    public Pbest() {
        Objective = 0;
        point = new Point[NUMPOINT];
    }

    public double getObjective() {
        return Objective;
    }
}

