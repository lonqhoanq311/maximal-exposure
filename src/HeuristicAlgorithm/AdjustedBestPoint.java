/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package HeuristicAlgorithm;

import data.Exposure;
import data.Grid;
import model.Location;
import model.NodeInfo;
import model.PathInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author longy
 */

public class    AdjustedBestPoint extends Heuristic {

    private float currTime;
    private float currLength;
    private float deltaS;
    private NodeInfo Start, Des;
    private Boolean[][] checkVer;

    public AdjustedBestPoint(){
        this(1);
    }

    public AdjustedBestPoint(float deltaS){
        super();
        setDeltaS(deltaS);
    }

    public float getDeltaS() {
        return deltaS;
    }

    public void setDeltaS(float deltaS) {
        if (deltaS<=0){
            this.deltaS = 1;
        } else {
            this.deltaS = deltaS;
        }
    }
    public void setCurrTime(float currTime) {
        this.currTime = currTime;
    }
    public void setCurrLength(float currLength) {
        this.currLength = currLength;
    }


    private void moveNode(Grid grid, NodeInfo preNode, NodeInfo postNode, NodeInfo adjustedNode){
//        Print.println("Move "+ adjustedNode.getLocation().toString());
        Location movedLoc;
        int[] xOffset = {0,0,-1,1};
        int[] yOffset = {1,-1,0,0};
        int currX = adjustedNode.getxGrid(), currY = adjustedNode.getyGrid(), nextX, nextY;

        float currTime = this.currTime - adjustedNode.getTimeMove()- postNode.getTimeMove();
        float currLength =  this.currLength - adjustedNode.getDistance()-postNode.getDistance();
        float timeToNew, lengthToNew, timeNewToPost, lengthNewToPost;

        float maxExp = Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(),adjustedNode.getLocation(), this.sensorNetwork.getMaxE());
        float tempExp;

        for (int i = 0; i < 4; i++) {
            nextX = currX + xOffset[i] ; nextY = currY + yOffset[i];
            if(0>=nextX || nextX>grid.getColumn()
            || 0>nextY || nextY>grid.getRow()){
                continue;
            }
            if (this.checkVer[nextX][nextY]){
                continue;
            }
            if((nextX==preNode.getxGrid()&&nextY==preNode.getyGrid())||(nextX==postNode.getxGrid()&&nextY==postNode.getyGrid())){
                continue;
            }
            if ((nextX == this.Start.getxGrid()&& nextY ==this.Start.getyGrid()) || (nextX == this.Des.getxGrid())&& nextY==this.Des.getyGrid()){
                continue;
            }

            movedLoc = grid.getVertices()[nextX][nextY];
            tempExp = Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(),grid.getVertices()[nextX][nextY],this.sensorNetwork.getMaxE());
//            Print.println("Considering "+ movedLoc.toString()+ " tempExp: "+ tempExp);
            if (tempExp > maxExp){
//                Print.println("MaxExp : " +maxExp);
                lengthToNew = (float)preNode.getLocation().euclidDistance(movedLoc);
                timeToNew = lengthToNew/this.sensorNetwork.getMaxSpeed();
                lengthNewToPost = (float)movedLoc.euclidDistance(postNode.getLocation());
                timeNewToPost = lengthNewToPost/this.sensorNetwork.getMaxSpeed();
                if (!lengthCondition(currLength, (lengthToNew+lengthNewToPost))){
                    continue;
                }
                if (!timeCondition(currTime,(timeToNew+timeNewToPost))){
                    continue;
                }
                adjustedNode.update(movedLoc, timeToNew, lengthToNew,nextX,nextY);
                postNode.setTimeMove(timeNewToPost);postNode.setDistance(lengthNewToPost);
                maxExp = tempExp;
                setCurrTime((currTime+timeToNew+timeNewToPost));setCurrLength((currLength+lengthToNew+lengthNewToPost));
            }
        }
        this.checkVer[adjustedNode.getxGrid()][adjustedNode.getyGrid()] = true;
//        Print.println("Moved to " +adjustedNode.getLocation().toString());
    }

    private void addNode(Grid grid, int preIndex, NodeInfo adjustedNode, NodeInfo postNode ) {
//        Print.println("Trying to add");
        Location addLoc;
        NodeInfo addNode = new NodeInfo();
        this.path.add(preIndex+1, addNode);

        int currX = adjustedNode.getxGrid(), currY = adjustedNode.getyGrid();
        int disX = postNode.getxGrid()-currX, disY = postNode.getyGrid()-currY;
        int nextX, nextY;
        int[] xOffset = {0,0,1,-1,disX, disX, disX+1, disX-1};
        int[] yOffset = {1,-1,0,0,disY+1, disY-1, disY, disY};

        float currTime = this.currTime - postNode.getTimeMove();
        float currLength =  this.currLength - postNode.getDistance();
        float timeToNew, lengthToNew, timeNewToPost, lengthNewToPost;
        boolean isAdded=false;

        float maxExp=0, tempExp;
        for (int i = 0; i < 8; i++) {
            nextX = currX + xOffset[i]; nextY  = currY + yOffset[i];
//            Print.println("nextX "+ nextX + " nextY "+ nextY);
            if(0>=nextX || nextX>grid.getColumn()
            || 0>nextY || nextY>grid.getRow()){
//                Print.println("continue because of bound");
                continue;
            }
            if (this.checkVer[nextX][nextY]){
                continue;
            }
            if((nextX==adjustedNode.getxGrid()&&nextY==adjustedNode.getyGrid())||(nextX==postNode.getxGrid()&&nextY==postNode.getyGrid())){
//                Print.println("continue because of repeat");
                continue;
            }
            if ((nextX == this.Start.getxGrid()&& nextY ==this.Start.getyGrid()) || (nextX == this.Des.getxGrid())&& nextY==this.Des.getyGrid()){
//                Print.println("continue because of head");
                continue;
            }
            addLoc = grid.getVertices()[nextX][nextY];
            tempExp = Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(),addLoc, this.sensorNetwork.getMaxE());
//            Print.println("Considering "+ addLoc.toString()+ " tempExp : "+ tempExp);
            if (tempExp>maxExp){
//                Print.println("MaxExp "+ maxExp);
                lengthToNew = (float )addLoc.euclidDistance(adjustedNode.getLocation());
                timeToNew = lengthToNew/this.sensorNetwork.getMaxSpeed();
                lengthNewToPost = (float)addLoc.euclidDistance(postNode.getLocation());
                timeNewToPost = lengthNewToPost/this.sensorNetwork.getMaxSpeed();
                if (!lengthCondition(currLength, (lengthToNew+lengthNewToPost))){
                    continue;
                }
                if (!timeCondition(currTime, (timeToNew+timeNewToPost))){
                    continue;
                }
                addNode.update(addLoc, timeToNew, lengthToNew, nextX,nextY);
                postNode.setTimeMove(timeNewToPost); postNode.setDistance(lengthNewToPost);
                maxExp = tempExp;
                setCurrTime((currTime + timeToNew + timeNewToPost));setCurrLength((currLength+lengthToNew+lengthNewToPost));
                isAdded = true;
//                Print.println("Add " + addLoc.toString());
            }
        }
        if (!isAdded){
//            Print.println("Cannot Add");
            this.path.remove(preIndex+1);
        } else {
            this.checkVer[addNode.getxGrid()][addNode.getyGrid()] = true;
        }
    }

    public void deleteNode(int index, NodeInfo preNode, NodeInfo postNode, NodeInfo adjustedNode, ArrayList<PathInfo> adjustedSubPath) throws CloneNotSupportedException {
//        Print.println("Deleting "+ adjustedNode.getLocation().toString());
        NodeInfo clonePostNode = postNode.clone();
        adjustedSubPath.add(preNode); adjustedSubPath.add(adjustedNode); adjustedSubPath.add(clonePostNode);
//        Print.println("Path Chosen To Try Delete");
//        printPath(adjustedSubPath);
        float maxExp = Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(), adjustedSubPath, this.sensorNetwork.getMaxE());
        float tempExp;
        float lengthToPost = (float)clonePostNode.getLocation().euclidDistance(preNode.getLocation());
        float timeToPost = lengthToPost/this.sensorNetwork.getMaxSpeed();
        clonePostNode.setTimeMove(timeToPost);clonePostNode.setDistance(lengthToPost);
        adjustedSubPath.remove(1);
//        Print.println("Path If Deleted");
//        printPath(adjustedSubPath);
        tempExp = Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(),adjustedSubPath, this.sensorNetwork.getMaxE());
//        Print.println("Current Exp" + maxExp + " tempEXP "+ tempExp);
        if (tempExp>maxExp){
            this.currTime = this.currTime - adjustedNode.getTimeMove()-postNode.getTimeMove()+ clonePostNode.getTimeMove();
            this.currLength = this.currLength - adjustedNode.getDistance() -postNode.getDistance() + clonePostNode.getDistance();
            this.path.remove(index);
            postNode.setTimeMove(timeToPost); postNode.setDistance(lengthToPost);
            this.checkVer[adjustedNode.getxGrid()][adjustedNode.getyGrid()] = false;
//            Print.println("Deleting");
        } else {
//            Print.println("Cannot delete\nPath stays still\n");
//            printPath(this.path);
        }
        adjustedSubPath.clear();
    }

    private void initializePath(Grid grid) throws CloneNotSupportedException {
        Random rd = new Random();
        int nextX=0, nextY=0;
        int yOffsetBound = grid.getRow()/5, xOffsetBound = grid.getColumn()/5, xOffset,yOffset;

        Location location = new Location();
        NodeInfo nodeInfo = new NodeInfo(0,0,location,0,0,0);
        nodeInfo.setLocation(location);
        for (int i=0; i<grid.getRow()/5;++i){
            xOffset = rd.nextInt(xOffsetBound);
            yOffset = rd.nextInt(yOffsetBound);
            if ((nextX + xOffset)>grid.getColumn()){
                nextX -= xOffset;
            } else {
                nextX += xOffset;
            }

            if ((nextY + yOffset)>grid.getRow()){
                nextY -= yOffset;
            } else {
                nextY += yOffset;
            }

//            Print.println("nextX "+nextX + " nextY "+ nextY);
            if(this.checkVer[nextX][nextY]){
                continue;
            }
            location = grid.getVertices()[nextX][nextY];
            nodeInfo.setLocation(location);
            if (Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(), location, this.sensorNetwork.getMaxE())!=0){
                nodeInfo.setxGrid(nextX);nodeInfo.setyGrid(nextY);
//                Print.println("try to add : "+ nextX+ ","+nextY);
                this.checkVer[nextX][nextY]=true;
                addSpecificNode(nodeInfo.clone());
            }
        }
//        Print.println("Path Initialize");
//        printPath(this.path);
    }

    private void addSpecificNode(NodeInfo addNode){
        int pos = this.path.size()-2;
        NodeInfo preNode = (NodeInfo) this.path.get(pos);
        float lengthToNew = (float)preNode.getLocation().euclidDistance(addNode.getLocation());
        float timeToNew =  lengthToNew/this.sensorNetwork.getMaxSpeed();
        float lengthNewToPost = (float) this.Des.getLocation().euclidDistance(addNode.getLocation());
        float timeNewToPost = lengthNewToPost/this.sensorNetwork.getMaxSpeed();
        float currTime = this.currTime-this.Des.getTimeMove();
        float currLength = this.currLength-this.Des.getDistance();
        if(!timeCondition(currTime, (timeToNew+timeNewToPost))){
            return;
        }
        if(!lengthCondition(currLength, (lengthToNew + lengthNewToPost))){
            return;
        }
        this.path.add(pos+1, addNode);
        addNode.setTimeMove(timeToNew);addNode.setDistance(lengthToNew);
        this.Des.setTimeMove(timeNewToPost); this.Des.setDistance(lengthNewToPost);
    }

    private void initialWithBestPoint(Grid grid){
        int rows = grid.getRow(), columns = grid.getColumn();
        NodeInfo addNode = new NodeInfo();
        float maxExp=0,tempExp;
        Location location, maxLoc = null;
        for (int row = 0; row <= rows; row++) {
            for(int column=0; column<=columns; column++){
                location=grid.getVertices()[row][column];
                tempExp = Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(), location, this.sensorNetwork.getMaxE());
                if (tempExp>maxExp){
                    maxExp=tempExp;
                    maxLoc = location;
                    addNode.setxGrid(row);addNode.setyGrid(column);
                }
            }
        }
        if (maxLoc != null){
            addNode.setLocation(maxLoc);
            addSpecificNode(addNode);
        }
    }

    @Override
    public float[] algorithm(String dataFile, float wOfField, float hOfField, float maxSpeed, float limitTime, float maxLength, float maxE) throws CloneNotSupportedException {
        long startTime = System.currentTimeMillis();

        try {
            sensorNetwork.initialFromFile(dataFile, wOfField, hOfField, maxSpeed,limitTime,maxLength, maxE);
        } catch (Exception e){
            return null ;
        }

        Grid grid = new Grid();
        grid.makeGrid(this.sensorNetwork, this.deltaS);
        this.checkVer =  new Boolean[grid.getRow()+1][grid.getColumn()+1];
        for (int i=0; i<=grid.getRow();i++){
            Arrays.fill(this.checkVer[i], Boolean.FALSE);
        }

        int tempX = Math.round(this.sensorNetwork.getStart().getX()/deltaS),tempY = Math.round(this.sensorNetwork.getStart().getY()/deltaS);
        this.Start = new NodeInfo(0,0,this.sensorNetwork.getStart(),0,tempY,tempX);
        this.path.add(this.Start);
        this.checkVer[tempY][tempX]=true;
        tempX = Math.round(this.sensorNetwork.getDest().getX()/deltaS); tempY = Math.round(this.sensorNetwork.getDest().getY()/deltaS);
        float lengthToNext = (float)this.sensorNetwork.getStart().euclidDistance(this.sensorNetwork.getDest());
        float timeToNext = lengthToNext/this.sensorNetwork.getMaxSpeed();
        this.Des = new NodeInfo((int)timeToNext,(int)lengthToNext,this.sensorNetwork.getDest(),0,tempY,tempX);
        this.path.add(this.Des);
        this.checkVer[tempY][tempX]=true;

        setCurrLength(lengthToNext);setCurrTime(timeToNext);

        initialWithBestPoint(grid);

        float currE = Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(),this.path, maxE),tempE= currE;
        float epsilon = 0.0001f;

        ArrayList<PathInfo> adjustedSubPath =  new ArrayList<>();
        NodeInfo preNode, adjustedNode, postNode;

        Random rd = new Random(); int choice, candidateNode, count=0 ;
        while (true){
            choice = rd.nextInt(3);
            if (this.path.size()==2){
                addNode(grid,0,Start,Des);
            } else {
                candidateNode = rd.nextInt(this.path.size()-2)+1;
                adjustedNode = (NodeInfo)this.path.get(candidateNode);
                preNode = (NodeInfo)this.path.get(candidateNode-1);
                postNode = (NodeInfo)this.path.get(candidateNode+1);
                if (choice==0){
                    moveNode(grid, preNode, postNode, adjustedNode);
                } else if (choice==1){
                    addNode(grid, candidateNode, adjustedNode, postNode);
                } else if (choice==2){
                    deleteNode(candidateNode, preNode, postNode, adjustedNode, adjustedSubPath);
                }
            }
            count++;
            if(count%20 == 0){
                currE = Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(), this.path, maxE);
                if (currE- tempE < epsilon){
//                    System.out.println("Break of Epsilon");
                    break;
                }
                tempE= currE;
            }
            if (this.currTime >= 20){
//                System.out.println("CurrTime "+ this.currTime);
//                System.out.println("TimeBound "+ (limitTime*50/100));
//                System.out.println("Break of Time");
                break;
            }
        }

        float remainTime = this.sensorNetwork.getLimitTime() - this.currTime;
        PathInfo maxNode = new PathInfo(), tempNode;
        float maxExp=0, tempExp;

//        Print.println("See Exp of each node in path");
        for (int i = 1; i < this.path.size()-1; i++) {
            tempNode = this.path.get(i);
            tempExp = Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(), tempNode.getLocation(), this.sensorNetwork.getMaxE());
//            Print.println(tempNode.getLocation().toString()+ " exp "+ tempExp);
            if (tempExp>=maxExp){
//                Print.println("temp is "+ tempExp + "maxExp is " +maxExp );
                maxNode = tempNode;
                maxExp =tempExp;
            }
        }

        maxNode.setTimeStayed(remainTime);
//        Print.println("MaxLoc "+ maxNode.getLocation().toString()+ " maxEXP : "+ maxExp);
        setExposure(Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(), this.path, maxE));

//        printPath(this.path);
        long endTime = System.currentTimeMillis();
        float runTime = (float) ((endTime - startTime));
//        Print.println("Curr Time "+ this.currTime + " currLength " + this.currLength);
        return new float[]{this.getExposure(), runTime};
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        String inputFolder = "./input/";

        int[] listNumberSensors = {10,20,50};
        int numOfTestEachCase = 3;//10;
        float exposure;
        float runTime;
        System.out.printf("%-20s \t %-10s \t %-10s \t %-10s \n","FILE","Exposure","NUM LOCATION","RUNTIME");
        for (int numberSensors : listNumberSensors) {
            for (int i = 1; i <= numOfTestEachCase; i++) {
                AdjustedBestPoint adjustedBestPoint = new AdjustedBestPoint(0.5f);
                String inputFile = inputFolder + numberSensors + "//" + numberSensors + "_" + i + ".txt";
                float[] result = adjustedBestPoint.algorithm(inputFile, 100, 100, 5,160 ,500, numberSensors/2);
                if(result==null){
                    continue;
                }
                exposure = result[0];
                runTime = result[1];
                System.out.printf("%-20s \t %-10f \t %-10d \t %-10d \n", "data_" + numberSensors + "_" + i + ".txt", exposure, adjustedBestPoint.getPath ().size(),(int) (runTime));
            }
        }
    }
}
