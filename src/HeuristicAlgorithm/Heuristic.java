/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package HeuristicAlgorithm;


import data.SensorNetwork;
import model.PathInfo;

import java.util.ArrayList;
/**
 *
 * @author longy
 */
public abstract class Heuristic {
    // Input
    protected SensorNetwork sensorNetwork;

    // Output
    protected ArrayList<PathInfo> path;
    protected float exposure;

    public Heuristic(){
        this.sensorNetwork = new SensorNetwork();
        this.path = new ArrayList<>();
        this.exposure = 0;
    }

    /**
     * getter
     * @return
     */
    public SensorNetwork getSensorNetwork() {
        return sensorNetwork;
    }
    public ArrayList<PathInfo> getPath() {
        return path;
    }
    public float getExposure() {
        return exposure;
    }

    /**
     * setter
     * @param sensorNetwork
     */
    public void setSensorNetwork(SensorNetwork sensorNetwork) {
        this.sensorNetwork = sensorNetwork;
    }
    public void setPath(ArrayList<PathInfo> path) {
        this.path = path;
    }
    public void setExposure(float exposure) {
        this.exposure = exposure;
    }

    public boolean lengthCondition(float currentLength, float remainLength){
        return (currentLength+remainLength) <= this.sensorNetwork.getMaxLength();
    }

    public boolean checkTimeWithRemainLength(float currTime, float lengthToDest){
        float shortestTimeToDest = lengthToDest/this.sensorNetwork.getMaxSpeed() + currTime;
        return shortestTimeToDest <= this.sensorNetwork.getLimitTime();
    }

    public boolean timeCondition(float currTime, float remainTime){
        return (currTime + remainTime <= this.sensorNetwork.getLimitTime());
    }

    public void printPath(ArrayList<PathInfo> path){
        int pathSize = path.size();
        for (int i=0; i<pathSize;++i){
            System.out.println(path.get(i).getLocation().toString());
            System.out.println("timeMove "+ path.get(i).getTimeMove()+" distance "+ path.get(i).getDistance()+" timeStayed "+ path.get(i).getTimeStayed());
        }
    }

    public abstract float[] algorithm(String dataFile,float wOfField,float hOfField, float maxSpeed, float limitTime, float maxLength, float maxE) throws CloneNotSupportedException;
}

