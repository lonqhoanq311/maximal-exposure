/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package HeuristicAlgorithm;

import data.Exposure;
import data.Print;
import model.Location;
import model.PathInfo;
/**
 *
 * @author longy
 */


public class ShortestPathHeuristic extends Heuristic{

    public ShortestPathHeuristic() {
        super();
    }

    @Override
    public float[] algorithm(String dataFile,float wOfField,float hOfField, float maxSpeed,float limitTime,float maxLength,float maxE){
        long startTime = System.currentTimeMillis();

        try {
            sensorNetwork.initialFromFile(dataFile, wOfField, hOfField, maxSpeed, limitTime, maxLength,maxE);
        } catch (Exception e){
            Print.println("Cannot Read Input File");
            return null ;
        }

        Location start = this.sensorNetwork.getStart();
        float startX = start.getX(); float startY  = start.getY();
        this.path.add(new PathInfo(start, 0,0,0));
        //System.out.println("Start Loc : "+ this.currLoc.getX() + " "+ this.currLoc.getY());
        Location dest = this.sensorNetwork.getDest();
        Location nextLoc = start, tempLoc;

        float disX = this.sensorNetwork.getDest().getX()-startX;
        float disY = this.sensorNetwork.getDest().getY()-startY;

        float currTime = 0, timeToNext, lengthToNext, lengthToDest = (float)start.euclidDistance(dest), timeToDest = lengthToDest/maxSpeed;

        float dis = (float)start.euclidDistance(dest);
        float maxCurrE=0;
        int degreeOfPrecision = Math.round(dis), i;
        if (degreeOfPrecision!=0) {
            disX = disX/degreeOfPrecision; disY = disY/degreeOfPrecision;
            for (i=1; i<degreeOfPrecision;++i){
                tempLoc = new Location(startX+i*disX, startY+i*disY);
                tempLoc.setExposure(Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(), tempLoc, maxE));
//                System.out.println("tempLoc : ("+tempLoc.getX()+", "+tempLoc.getY()+" ), exposure: "+tempLoc.getExposure());
                if (maxCurrE<=tempLoc.getExposure()){
                    nextLoc = tempLoc;
                    maxCurrE = tempLoc.getExposure();
                }
            }

            lengthToNext =  (float)start.euclidDistance(nextLoc);
            lengthToDest = (float)nextLoc.euclidDistance(dest);

            timeToNext = lengthToNext/maxSpeed;
            currTime += timeToNext;
            timeToDest = lengthToDest/maxSpeed;
            if (checkTimeWithRemainLength(currTime,lengthToDest)) {
                this.path.add(new PathInfo(nextLoc,timeToNext,lengthToNext,(limitTime-currTime-timeToDest)));
            }
        }

        this.path.add(new PathInfo(dest, timeToDest, lengthToDest, 0));
        if (!checkTimeWithRemainLength(currTime,lengthToDest)){
            Print.println("Cannot Reach Destination On TIme");
            return null;
        }


        long endTime = System.currentTimeMillis();
        float runTime = (float) ((endTime - startTime));
        this.setExposure(Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(),this.path, maxE));
//        Print.println("Exceptional Test");
//        Print.println("Original Path: "+ this.getExposure());
//        PathInfo middle = this.path.get(1);
//        middle.setTimeStayed((middle.getTimeStayed()/2));
//        PathInfo middle2 = new PathInfo(middle.getLocation(),0,0, middle.getTimeStayed());
//        this.path.add(2, middle2);
//        Print.println("Test Path : "+ Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(), this.path, maxE));
//        printPath(this.path);
        return new float[]{this.getExposure(), runTime};
    }

    public static void main(String[] args){
        String inputFolder = "./input/";

        int[] listNumberSensors = {10,20,50};
        int numOfTestEachCase = 3;//10;
        float exposure;
        float runTime;
        Print.printf("%-20s \t %-10s \t %-10s \t %-10s \n","FILE","Exposure","NUM SENSOR","RUNTIME");
        for (int numberSensors : listNumberSensors) {
            for (int i = 1; i <= numOfTestEachCase; i++) {
                ShortestPathHeuristic shortestPathHeuristic = new ShortestPathHeuristic();
                String inputFile = inputFolder + numberSensors + "//" + numberSensors + "_" + i + ".txt";
                float[] result = shortestPathHeuristic.algorithm(inputFile, 100, 100, 5,100 ,500, numberSensors/2);
                if(result==null){
                    continue;
                }
                exposure = result[0];
                runTime = result[1];
                Print.printf("%-20s \t %-10f \t %-10d \t %-10d \n", "data_" + numberSensors + "_" + i + ".txt", exposure, shortestPathHeuristic.getPath ().size(),(int) (runTime));
            }
        }
    }
}
