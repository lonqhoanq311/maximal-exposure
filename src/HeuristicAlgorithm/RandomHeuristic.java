/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package HeuristicAlgorithm;

import data.Exposure;
import data.Print;
import model.Location;
import model.PathInfo;

import java.util.Random;

/**
 *
 * @author longy
 */

public class RandomHeuristic extends Heuristic{

    public RandomHeuristic() {
        super();
    }

    public Location randomLocation(){
        Random rd = new Random();
        float newX = rd.nextFloat()*this.sensorNetwork.getWOfField();
        float newY = rd.nextFloat()*this.sensorNetwork.getHOfField();
        return (new Location(newX, newY));
    }

    // choose a random Location on shortest Path to Des
    public Location shortestLocation(Location currLoc, float desX, float desY){
        Random rd = new Random();
        float randomLocation = rd.nextFloat();
        while((randomLocation==0)||(randomLocation==1)){
            randomLocation = rd.nextFloat();
        }
        return (new Location(randomLocation*(desX-currLoc.getX()), randomLocation*(desY - currLoc.getY())));
    }

    @Override
    public float[] algorithm(String dataFile,float wOfField,float hOfField, float maxSpeed, float limitTime, float maxLength, float maxE){
        long startTime = System.currentTimeMillis();

        try {
            sensorNetwork.initialFromFile(dataFile, wOfField, hOfField, maxSpeed,limitTime,maxLength, maxE);
            // 100, 100, 5, 1, 100.
        } catch (Exception e){
            Print.println("Cannot Read Input File");
            return null ;
        }

        Location currLoc = this.sensorNetwork.getStart();

        this.path.add(new PathInfo(currLoc, 0, 0, 0));
//        System.out.println("Start Loc : "+ this.currLoc.getX() + " "+ this.currLoc.getY());
        Location nextLoc = null;
        Location des = this.sensorNetwork.getDest();
        float desX = des.getX();
        float desY = des.getY();
        boolean isChosen ;

        float currTime=0, currLength=0, lengthToNext=0 , timeToNext, lengthToDest;
        float percentageTime = (0.3f)*100;
        int timeChooseRandom = 5, i;

//        this.sensorNetwork.printInfo();
        while((limitTime-currTime)>percentageTime){
            isChosen = false;
            for (i=0;i<timeChooseRandom;++i){

                nextLoc = randomLocation();
                lengthToNext = (float)currLoc.euclidDistance(nextLoc);
                lengthToDest = lengthToNext + (float)nextLoc.euclidDistance(des);
//                System.out.println("Checking Random");
//                System.out.println("CurrLength "+ currLength +" lengthToDest "+ lengthToDest);
                if (lengthCondition(currLength, lengthToDest)) {
//                    System.out.println("Pass Length Cond");
                    if (checkTimeWithRemainLength(currTime, lengthToDest)){
//                    System.out.println("Random Loc : "+ nextLoc.getX()+" " + nextLoc.getY());
                        isChosen = true;
                        break;
                    }
                }
            }
            if (!isChosen){
//                System.out.println("Random Failed");
                nextLoc = shortestLocation(currLoc,desX, desY);
                lengthToNext = (float)nextLoc.euclidDistance(currLoc);
            }

            timeToNext = lengthToNext/maxSpeed;
            this.path.add(new PathInfo(nextLoc,timeToNext, lengthToNext, 0 ));
            //this.setCurrTime(this.currTime + (float) nextLoc.euclidDistance(currLoc)/this.sensorNetwork.getMaxSpeed());
            currTime = currTime + timeToNext;
            currLength = currLength + lengthToNext;
            currLoc  = nextLoc;

        }
        nextLoc = this.sensorNetwork.getDest();
        lengthToDest = (float)currLoc.euclidDistance(nextLoc);
        if (!checkTimeWithRemainLength(currTime, lengthToDest)){
            Print.println("CurrTime : "+ currTime + " Time needed "+ (lengthToDest/maxSpeed));
            Print.println("Cannot Reach Destination On Time");
            return null;
        }

//        System.out.println("Des Loc : "+ nextLoc.getX() + " "+ nextLoc.getY());

        this.path.add(new PathInfo(nextLoc,lengthToDest/maxSpeed, lengthToDest, 1 ));

        long endTime = System.currentTimeMillis();
        float runTime = (float) ((endTime - startTime));
        this.setExposure(Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(), this.path, maxE));
        return new float[]{this.getExposure(), runTime};
    }

    public static void main(String[] args){
        String inputFolder = "input//";

        int[] listNumberSensors = {10,20,50};
        int numOfTestEachCase = 3;//10;
        float exposure;
        float runTime;
        Print.printf("%-20s \t %-10s \t %-10s \t %-10s \t %-10s \n","FILE","Exposure","NUM SENSOR","RUNTIME","TIMETEST");
        for (int numberSensors : listNumberSensors) {
            for (int i = 1; i <= numOfTestEachCase; i++) {
                int j = 1;
                while (j <=10) {
                    RandomHeuristic randomHeuristic = new RandomHeuristic();
                    String inputFile = inputFolder + numberSensors + "//" + numberSensors + "_" + i + ".txt";
                    float[] result = randomHeuristic.algorithm(inputFile, 100, 100, 5,100, 500, numberSensors/2);
                    if(result==null){
                        continue;
                    }
                    exposure = result[0];
                    runTime = result[1];
                    Print.printf("%-20s \t %-10f \t %-10d \t %-10d \t %-10d \n", "data_" + numberSensors + "_" + i + ".txt", exposure, randomHeuristic.getPath ().size(),(int) (runTime),j);
                    j++;
                }
                System.out.println("=========================================================================");
            }
        }
    }
}
