/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package HeuristicAlgorithm;

import data.Exposure;
import data.Grid;

import data.Print;
import model.Location;
import model.NodeInfo;
import model.PathInfo;
/**
 *
 * @author longy
 */


import java.util.ArrayList;

public class BestPointHeuristic extends Heuristic{

    private float currTime;
    private float currLength;
    private NodeInfo Start, Des;

    private float deltaS;

    public BestPointHeuristic(){
        this(1);
    }

    public BestPointHeuristic(float deltaS){
        super();
        setDeltaS(deltaS);
    }

    public float getDeltaS() {
        return deltaS;
    }

    public void setDeltaS(float deltaS) {
        if (deltaS<=0){
            this.deltaS = 1;
        } else {
            this.deltaS = deltaS;
        }
    }

    private void addSpecificNode(NodeInfo addNode){
        int pos = this.path.size()-2;
        NodeInfo preNode = (NodeInfo) this.path.get(pos);
        float lengthToNew = (float)preNode.getLocation().euclidDistance(addNode.getLocation());
        float timeToNew =  lengthToNew/this.sensorNetwork.getMaxSpeed();
        float lengthNewToPost = (float) this.Des.getLocation().euclidDistance(addNode.getLocation());
        float timeNewToPost = lengthNewToPost/this.sensorNetwork.getMaxSpeed();
        float currTime = this.currTime-this.Des.getTimeMove();
        float currLength = this.currLength-this.Des.getDistance();
        if(!timeCondition(currTime, (timeToNew+timeNewToPost))){
            return;
        }
        if(!lengthCondition(currLength, (lengthToNew + lengthNewToPost))){
            return;
        }
        this.path.add(pos+1, addNode);
        addNode.setTimeMove(timeToNew);addNode.setDistance(lengthToNew);
        this.Des.setTimeMove(timeNewToPost); this.Des.setDistance(lengthNewToPost);
    }

    private void initialWithBestPoint(Grid grid){
        int rows = grid.getRow(), columns = grid.getColumn();
        NodeInfo addNode = new NodeInfo();
        float maxExp=0,tempExp;
        Location location, maxLoc = null;
        for (int row = 0; row <= rows; row++) {
            for(int column=0; column<=columns; column++){
                location=grid.getVertices()[row][column];
                tempExp = Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(), location, this.sensorNetwork.getMaxE());
                if (tempExp>maxExp){
                    maxExp=tempExp;
                    maxLoc = location;
                    addNode.setxGrid(row);addNode.setyGrid(column);
                }
            }
        }
        if (maxLoc != null){
            addNode.setLocation(maxLoc);
            addSpecificNode(addNode);
        }
    }

    @Override
    public float[] algorithm(String dataFile,float wOfField,float hOfField, float maxSpeed,float limitTime,float maxLength,float maxE){
        long startTime = System.currentTimeMillis();

        try {
            sensorNetwork.initialFromFile(dataFile, wOfField, hOfField, maxSpeed, limitTime, maxLength,maxE);
        } catch (Exception e){
            System.out.println("Cannot Read Input File");
            return null ;
        }

        Grid grid = new Grid();
        grid.makeGrid(this.sensorNetwork, this.deltaS);
        Location vertices[][] = grid.getVertices();
        int rows = grid.getRow(), columns = grid.getColumn();
        PathInfo start = new PathInfo(this.sensorNetwork.getStart(),0,0,0);
        PathInfo des = new PathInfo(this.sensorNetwork.getDest(),0,0,0);
        PathInfo bestPoint = new PathInfo();
        Location bestLocation ;
        ArrayList<PathInfo> tempPath= new ArrayList<>();
        tempPath.add(start); tempPath.add(bestPoint);tempPath.add(des);

        float lengthToNew, timeToNew, lengthNewToDes, timeNewToDes;
        float maxExp=0, tempExp;
        for (int row=0; row<=rows;row++){
            for (int column=0; column<columns;column++){
                bestLocation = grid.getVertices()[row][column];
                lengthToNew = (float)start.getLocation().euclidDistance(bestLocation);
                timeToNew = lengthToNew/this.sensorNetwork.getMaxSpeed();
                lengthNewToDes = (float)des.getLocation().euclidDistance(bestLocation);
                timeNewToDes = lengthNewToDes/this.sensorNetwork.getMaxSpeed();
                if (!lengthCondition(0,(lengthNewToDes+lengthToNew))){
                    continue;
                }
                if (!timeCondition(0,(timeNewToDes+timeToNew))){
                    continue;
                }
                bestPoint.setLocation(bestLocation);bestPoint.setTimeMove(timeToNew);bestPoint.setDistance(lengthToNew);bestPoint.setTimeStayed(limitTime-timeToNew-timeNewToDes);
                des.setTimeMove(timeNewToDes); des.setDistance(lengthNewToDes);
//                tempExp = Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(),tempPath,maxE );
                tempExp = Exposure.computeBiExposure(this.sensorNetwork.getListSensors(),tempPath);
//                tempExp = Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(), bestLocation, maxE);
                if (tempExp>maxExp){
//                    bestPoint.setLocation(bestLocation);bestPoint.setTimeMove(timeToNew);bestPoint.setDistance(lengthToNew);bestPoint.setTimeStayed(limitTime-timeToNew-timeNewToDes);
//                    des.setTimeMove(timeNewToDes); des.setDistance(lengthNewToDes);
                    setPath(tempPath);
                    maxExp=tempExp;
                    setExposure(maxExp);
                }
            }

        }

//        int tempX = Math.round(this.sensorNetwork.getStart().getX()/deltaS),tempY = Math.round(this.sensorNetwork.getStart().getY()/deltaS);
//        this.Start = new NodeInfo(this.sensorNetwork.getStart(),0,0,0,tempY,tempX);
//        this.path.add(this.Start);
//
//        tempX = Math.round(this.sensorNetwork.getDest().getX()/deltaS); tempY = Math.round(this.sensorNetwork.getDest().getY()/deltaS);
//        float lengthToNext = (float)this.sensorNetwork.getStart().euclidDistance(this.sensorNetwork.getDest());
//        float timeToNext = lengthToNext/this.sensorNetwork.getMaxSpeed();
//        this.Des = new NodeInfo(this.sensorNetwork.getDest(),timeToNext,lengthToNext,0,tempY,tempX);
//        this.path.add(this.Des);
//
//        this.currLength= lengthToNext; this.currTime = timeToNext;
//
//        initialWithBestPoint(grid);
//        float remainTime = this.sensorNetwork.getLimitTime() - this.currTime;
//        PathInfo maxNode = new PathInfo(), tempNode;
//        float maxExp=0, tempExp;
//
////        Print.println("See Exp of each node in path");
//        for (int i = 1; i < this.path.size()-1; i++) {
//            tempNode = this.path.get(i);
//            tempExp = Exposure.sumNonBiExposure(this.sensorNetwork.getListSensors(), tempNode.getLocation(), this.sensorNetwork.getMaxE());
////            Print.println(tempNode.getLocation().toString()+ " exp "+ tempExp);
//            if (tempExp>=maxExp){
////                Print.println("temp is "+ tempExp + "maxExp is " +maxExp );
//                maxNode = tempNode;
//                maxExp =tempExp;
//            }
//        }
//
//        maxNode.setTimeStayed(remainTime);
//        setExposure(Exposure.computeNonBiExposure(this.sensorNetwork.getListSensors(), this.path,maxE));
//        printPath(this.path);

        Print.println(this.path.get(1).getLocation().toString());
        long endTime = System.currentTimeMillis();
        float runTime = (float) ((endTime - startTime));

        return new float[]{this.getExposure(), runTime};
    }

    public static void main(String[] args){
        String inputFolder = "./input/";

        int[] listNumberSensors = {10,20,50};
        int numOfTestEachCase = 3;//10;
        float exposure;
        float runTime;
        System.out.printf("%-20s \t %-10s \t %-10s \t %-10s \n","FILE","Exposure","NUM SENSOR","RUNTIME");
        for (int numberSensors : listNumberSensors) {
            for (int i = 1; i <= numOfTestEachCase; i++) {
                BestPointHeuristic bestPointHeuristic = new BestPointHeuristic(0.25f);
                String inputFile = inputFolder + numberSensors + "//" + numberSensors + "_" + i + ".txt";
//                String inputFile = inputFolder + numberSensors + "/data_sample.txt";
                float[] result = bestPointHeuristic.algorithm(inputFile, 100, 100, 5,100 ,500, numberSensors/2);
                if(result==null){
                    continue;
                }
                exposure = result[0];
                runTime = result[1];
                System.out.printf("%-20s \t %-10f \t %-10d \t %-10d \n", "data_" + numberSensors + "_" + i + ".txt", exposure, bestPointHeuristic.getPath ().size(),(int) (runTime));
            }
        }
    }
}
