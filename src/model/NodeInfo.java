/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author longy
 */
public class NodeInfo extends PathInfo implements Cloneable{
    public int xGrid;
    public int yGrid;

   
    public NodeInfo(int xGrid, int yGrid, Location location, float timeMove, float distance, float timeStayed) {
        super(location, timeMove, distance, timeStayed);
        this.xGrid = xGrid;
        this.yGrid = yGrid;
    }
     public NodeInfo(){
        this(0,0,new Location(),0,0,0);
    }
    

    public int getxGrid() {
        return xGrid;
    }

    public void setxGrid(int xGrid) {
        this.xGrid = xGrid;
    }

    public int getyGrid() {
        return yGrid;
    }

    public void setyGrid(int yGrid) {
        this.yGrid = yGrid;
    }
    
    public void update(Location location, float timeMove, float distance, int nextX, int nextY){
        setLocation(location);
        setTimeMove(timeMove);
        setDistance(distance);
        setxGrid(nextX);
        setyGrid(nextY);
    }
    
    public void update(Location location, float timeMove, float distance,float timeStayed, int nextX, int nextY){
        update(location, timeMove, distance, nextX, nextY);
        setTimeStayed(timeStayed);
    }
    
    @Override
    public String toString() {
        return ("Grid Coor: " + this.xGrid + " ,"+this.yGrid);
    }

    /**
     *
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    public NodeInfo clone() throws CloneNotSupportedException{
        try{
            NodeInfo cloneNode = (NodeInfo)super.clone();
            cloneNode.setLocation(new Location(this.getLocation().getX(),this.getLocation().getY()));
            return cloneNode;
        } catch (CloneNotSupportedException e){
            System.err.println("Error: " + e.getMessage());
            return null;
        }
    }
}
