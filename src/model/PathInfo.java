/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author longy
 */
public class PathInfo implements Cloneable{
    private Location location;
    private float timeMove;
    private float distance;
    private float timeStayed;

    public PathInfo(Location location, float timeMove, float distance, float timeStayed) {
        this.location = location;
        this.timeMove = timeMove;
        this.distance = distance;
        this.timeStayed = timeStayed;
    }
    
    public PathInfo(){
        this(new Location(),0,0,0);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public float getTimeMove() {
        return timeMove;
    }

    public void setTimeMove(float timeMove) {
        this.timeMove = timeMove;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getTimeStayed() {
        return timeStayed;
    }

    public void setTimeStayed(float timeStayed) {
        this.timeStayed = timeStayed;
    }
    
    /**
     *
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    public PathInfo clone() throws CloneNotSupportedException {
        try {
            PathInfo cloneNode = (PathInfo) super.clone();
            cloneNode.setLocation(new Location(this.getLocation().getX(), this.getLocation().getY()));
            return cloneNode;
        } catch (CloneNotSupportedException e) {
            System.err.println("Error: " + e.getMessage());
            return null;
        }
    }

    @Override
    public String toString() {
        return this.location.toString() + " "+ "time_move: "+ this.timeMove + " distance: "+ this.distance+ " time_stay: "+ this.timeStayed;
    }
    
    
}
