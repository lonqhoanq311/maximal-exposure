/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package data;

import model.Location;

/**
 *
 * @author longy
 */
public class Grid {
    public Location vertices[][]; // vertices of grid
    public int row; // numbers of rows of grid
    public int column; // numbers of columns of grid

    public void setRow(int row) {
        this.row = row;
    }
    public void setColumn(int column) {
        this.column = column;
    }
    public void setVertices(Location[][] vertices) {
        this.vertices = vertices;
    }

    public int getRow() {
        return row;
    }
    public int getColumn() {
        return column;
    }
    public Location[][] getVertices() {
        return vertices;
    }

    public void makeGrid(SensorNetwork sensorNetwork, float deltaS) {
        int columnGrid = Math.round(sensorNetwork.getWOfField() / deltaS); // number of columns-1
        int rowGrid = Math.round(sensorNetwork.getHOfField() / deltaS); // number of rows-1
        Location[][] verticesGridLocationses = new Location[rowGrid + 1][columnGrid + 1];
        for (int i = 0; i <= rowGrid; i++) {
            for (int j = 0; j <= columnGrid; j++) {
                Location newLoc = new Location(j * deltaS, i * deltaS);
                verticesGridLocationses[i][j] = newLoc;
            }
        }

        this.setColumn(columnGrid);
        this.setRow(rowGrid);
        this.setVertices(verticesGridLocationses);
    }
}
