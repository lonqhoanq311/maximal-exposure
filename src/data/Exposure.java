/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package data;
import java.util.ArrayList;
import model.Sensor;
import model.Location;
import model.PathInfo;

/**
 *
 * @author longy
 */
public class Exposure {
    public static float binaryExposureAtPoint(Sensor sensor, Location location){
        return location.euclidDistance(sensor) > sensor.getR() ? 0 : 1;
    }

    public static float sumBinaryExposure(ArrayList<Sensor> listSensors, Location location){
        float sumE = 0;
        for (Sensor sensor : listSensors)
            sumE += binaryExposureAtPoint(sensor, location);
        return  sumE;
    }

    public static float nonBiExposureAtPoint(Sensor sensor, Location location, float maxE){
        double distance= location.euclidDistance(sensor);
        if (distance < 1/maxE){
            distance = 1/maxE;
        }
        if(distance > sensor.getR()){
            return 0;
        }
        else{
            return (float) ((float) 1/distance);
        }
    }

    public static float sumNonBiExposure(ArrayList<Sensor> listSensors, Location location, float maxE){
        float sumE = 0;
        for (Sensor sensor : listSensors)
            sumE += nonBiExposureAtPoint(sensor, location, maxE);
        return sumE;
    }

    public static float sumExposure(ArrayList<Sensor> listSensors, Location location, float maxE){
        float sumE;
        if (maxE==0){
            sumE = sumBinaryExposure(listSensors, location);
        } else {
            sumE = sumNonBiExposure(listSensors, location, maxE);
        }
        return sumE;
    }

    public static float sumExposure(SensorNetwork sensorNetwork, Location location){
        return sumExposure(sensorNetwork.getListSensors(), location, sensorNetwork.getMaxE());
    }

    private static float computeBiExposure(ArrayList<Sensor> listSensors, Location firstPoint, PathInfo secondPoint){

        float dis = secondPoint.getDistance();
        int degreeOfPrecision = Math.round(dis);

//        System.out.println("First Point : "+firstPoint.toString()+" . Second Point: "+ secondPoint.toString());
        if (degreeOfPrecision == 0) {
            return ((sumBinaryExposure(listSensors, firstPoint)+ sumBinaryExposure(listSensors, secondPoint.getLocation()))/2)*secondPoint.getTimeMove();
        } else {

            float xFirst = firstPoint.getX();
            float yFirst = firstPoint.getY();
            Location second = secondPoint.getLocation();
            float xDis = second.getX() - xFirst;
            float yDis = second.getY()- yFirst;
            float deltaT = secondPoint.getTimeMove()/degreeOfPrecision;
            Location tempLoc;
            float segmentExp=0;

            float partDisX = xDis/degreeOfPrecision;
            float partDisY = yDis/degreeOfPrecision;
            tempLoc = new Location();
            for (int i = 1; i < degreeOfPrecision; ++i) {
                tempLoc.setX(xFirst + i * partDisX); tempLoc.setY(yFirst + i * partDisY);
                segmentExp += sumBinaryExposure(listSensors, tempLoc) * deltaT;
//                tempLoc.setExposure(sumBinaryExposure(listSensors,tempLoc));
//                System.out.println(tempLoc.toString()+" exposure: "+ tempLoc.getExposure());
            }
            segmentExp += sumBinaryExposure(listSensors, second)*(deltaT+secondPoint.getTimeStayed());
            return segmentExp;
        }
    }

    public static float computeNonBiExposure(ArrayList<Sensor> listSensors, Location firstPoint, PathInfo secondPoint,float maxE){

        float dis = secondPoint.getDistance();
        int degreeOfPrecision = Math.round(dis);

        if (degreeOfPrecision == 0) {
//            return ((sumNonBiExposure(listSensors, firstPoint, maxE)+ sumNonBiExposure(listSensors, secondPoint.getLocation(),maxE))/2)*(secondPoint.getTimeMove()+secondPoint.getTimeStayed());
//            return (sumNonBiExposure(listSensors, firstPoint, maxE)+ sumNonBiExposure(listSensors, secondPoint.getLocation(), maxE)*(secondPoint.getTimeStayed()+secondPoint.getTimeMove()));
            return (sumNonBiExposure(listSensors, secondPoint.getLocation(), maxE)*(secondPoint.getTimeStayed()+secondPoint.getTimeMove()));
        } else {
            float xFirst = firstPoint.getX();
            float yFirst = firstPoint.getY();
            Location second = secondPoint.getLocation();
            float xDis = second.getX() - xFirst;
            float yDis = second.getY()- yFirst;
            float deltaT = secondPoint.getTimeMove()/degreeOfPrecision;
            Location tempLoc;
            float segmentExp=0;

            float partDisX = xDis/degreeOfPrecision;
            float partDisY = yDis/degreeOfPrecision;
            tempLoc = new Location();
            for (int i = 1; i < degreeOfPrecision; ++i) {
                tempLoc.setX(xFirst + i * partDisX); tempLoc.setY(yFirst + i * partDisY);
                segmentExp += sumNonBiExposure(listSensors, tempLoc,maxE) * deltaT;
            }
            segmentExp += sumNonBiExposure(listSensors, second, maxE)*(deltaT+secondPoint.getTimeStayed());

            return segmentExp;
        }
    }

    public static float computeBiExposure(ArrayList<Sensor> listSensors, ArrayList<PathInfo> path){
        float sumExposure = 0;
        int pathSize = path.size();
//        sumExposure+= sumBinaryExposure(listSensors, path.get(0).getLocation());
        for (int i=1; i<pathSize;++i){
            sumExposure += computeBiExposure(listSensors, path.get(i-1).getLocation(), path.get(i));
        }
        return sumExposure;
    }

    public static float computeNonBiExposure(ArrayList<Sensor> listSensors, ArrayList<PathInfo> path, float maxE){
        float sumExposure = 0;
        int pathSize= path.size();
        sumExposure+= sumNonBiExposure(listSensors, path.get(0).getLocation(), maxE);
        for (int i=1; i<pathSize;++i){
            sumExposure += computeNonBiExposure(listSensors, path.get(i-1).getLocation(), path.get(i), maxE);
        }
        return sumExposure;
    }

    /**
     *
     * @param sensorNetwork
     * @param path
     * @return
     */
    public static float computeExposure(SensorNetwork sensorNetwork, ArrayList<PathInfo> path){
        float maxE = sensorNetwork.getMaxE();
        if(maxE == 0){
            return computeBiExposure(sensorNetwork.getListSensors(), path);
        } else {
            return computeNonBiExposure(sensorNetwork.getListSensors(), path, maxE);
        }
    }

    public static float displayComputeNonBiExposure(ArrayList<Sensor> listSensors, Location firstPoint, PathInfo secondPoint,float maxE){
        float dis = secondPoint.getDistance();
        int degreeOfPrecision = Math.round(dis);

//        System.out.println("First Point : "+firstPoint.toString()+" . Second Point: "+ secondPoint.toString());
        if (degreeOfPrecision == 0) {
            return ((sumNonBiExposure(listSensors, firstPoint, maxE)+ sumNonBiExposure(listSensors, secondPoint.getLocation(),maxE))/2)*(secondPoint.getTimeMove()+secondPoint.getTimeStayed());
        } else {
            float xFirst = firstPoint.getX();
            float yFirst = firstPoint.getY();
            Location second = secondPoint.getLocation();
            float xDis = second.getX() - xFirst;
            float yDis = second.getY()- yFirst;
            float deltaT = secondPoint.getTimeMove()/degreeOfPrecision;
            Location tempLoc;
            float segmentExp=0;

            float partDisX = xDis/degreeOfPrecision;
            float partDisY = yDis/degreeOfPrecision;

            tempLoc = new Location();
            for (int i = 1; i < degreeOfPrecision; ++i) {
                tempLoc.setX(xFirst + i * partDisX);tempLoc.setY(yFirst + i * partDisY);
                segmentExp += sumNonBiExposure(listSensors, tempLoc,maxE) * deltaT;
                System.out.println("Segment EXP "+ segmentExp);
//                tempLoc.setExposure(sumNonBiExposure(listSensors,tempLoc,maxE));
//                System.out.println(tempLoc.toString()+" exposure: "+ tempLoc.getExposure());
            }
            segmentExp += sumNonBiExposure(listSensors, second, maxE)*(deltaT+secondPoint.getTimeStayed());
            System.out.println("Segment EXP "+ segmentExp);
            return segmentExp;
        }
    }

    public static float displayComputeNonBiExposure(ArrayList<Sensor> listSensors, ArrayList<PathInfo> path, float maxE){
        float sumExposure = 0;
        int pathSize= path.size();
        sumExposure+= sumNonBiExposure(listSensors, path.get(0).getLocation(), maxE)*(1+path.get(0).getTimeStayed());
        for (int i=1; i<pathSize;++i) {
            System.out.println("Path from " + path.get(i-1).getLocation().toString()+ " to " + path.get(i).getLocation().toString());
            sumExposure += displayComputeNonBiExposure(listSensors, path.get(i-1).getLocation(), path.get(i), maxE);
        }
        return sumExposure;
    }
}
