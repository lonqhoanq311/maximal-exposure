/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package data;

import model.Location;
import model.Sensor;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author longy
 */
public class SensorNetwork {
    public float wOfField, hOfField; // width and height of sensor network field.
    public int numOfSensors;
    public ArrayList<Sensor> listSensors;
    public float maxSpeed;
    public float limitTime;
    public float maxLength;
    public float maxE; // maximum exposure on each location
    public Location start;
    public Location dest;

    /**
     * getter
     */
    public float getWOfField() {
        return wOfField;
    }
    public float getHOfField() {
        return hOfField;
    }
    public int getNumOfSensors() {
        return numOfSensors;
    }
    public ArrayList<Sensor> getListSensors() {
        return listSensors;
    }
    public float getMaxSpeed() {
        return maxSpeed;
    }
    public float getLimitTime() {
        return limitTime;
    }
    public float getMaxLength() {
        return maxLength;
    }
    public float getMaxE() {
        return maxE;
    }
    public Location getStart() {
        return start;
    }
    public Location getDest() {
        return this.dest;
    }

    /**
     * setter
     */
    public void setWOfField(float wOfField) {
        this.wOfField = wOfField;
    }
    public void setHOfField(float hOfField) {
        this.hOfField = hOfField;
    }
    public void setNumOfSensors(int numOfSensors) {
        this.numOfSensors = numOfSensors;
    }
    public void setListSensors(ArrayList<Sensor> listSensors) {
        this.listSensors = listSensors;
    }
    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
    public void setLimitTime(float limitTime) {
        this.limitTime = limitTime;
    }
    public void setMaxLength(float maxLength) {
        this.maxLength = maxLength;
    }
    public void setMaxE(float maxE) {
        this.maxE = maxE;
    }
    public void setStart(Location start) {
        this.start = start;
    }
    public void setDest(Location dest) {
        this.dest = dest;
    }

    public void randomInitial(float wOfField, float hOfField, int numOfSensors, float maxRadius, float maxSpeed,
                              float limitTime,float maxLength, float maxE) {
        setWOfField(wOfField);
        setHOfField(hOfField);
        setNumOfSensors(numOfSensors);
        setMaxSpeed(maxSpeed);
        setLimitTime(limitTime);
        setMaxLength(maxLength);
        setMaxE(maxE);

        Random rand = new Random();
        setStart(new Location((float) (Math.round(wOfField * rand.nextFloat() / 0.5) * 0.5), 0));
        setDest(new Location((float) (Math.round(wOfField * rand.nextFloat() / 0.5) * 0.5), this.hOfField));

        setListSensors(new ArrayList<>());
        for (int i = 0; i < numOfSensors; i++)
            this.listSensors.add(new Sensor(wOfField * rand.nextFloat(), hOfField * rand.nextFloat(),
                    maxRadius * rand.nextFloat()));
    }

    public void initialFromFile(String file,float wOfField,float hOfField, float maxSpeed, float limitTime, float maxLength, float maxE) {
        FileReader inFileReader;
        BufferedReader in;
        try {
            inFileReader = new FileReader(file);
            in = new BufferedReader(inFileReader);
            String[] line;
            line = in.readLine().split(" ");
            setNumOfSensors(Integer.parseInt(line[0]));
            in.readLine();
            this.listSensors = new ArrayList<>();
            for (int i = 0; i < this.numOfSensors; i++) {
                line = in.readLine().split(" ");
                float x = Float.parseFloat(line[0]);
                float y = Float.parseFloat(line[1]);
                float z = Float.parseFloat(line[2]);
                this.listSensors.add(new Sensor(x, y, z));
            }

            line = in.readLine().split(" ");
            setStart(new Location(Float.parseFloat(line[0]), Float.parseFloat(line[1])));

            line = in.readLine().split(" ");
            setDest(new Location(Float.parseFloat(line[0]), Float.parseFloat(line[1])));

            in.close();

            basicInitial(wOfField,hOfField, maxSpeed, limitTime, maxLength, maxE);
        } catch (NumberFormatException  | IOException e) {
            System.out.println("Error When Read File To Setup Sensor Network");
        }

    }

    public void basicInitial(float wOfField,float hOfField, float maxSpeed, float limitTime, float maxLength, float maxE){
        setWOfField(wOfField);
        setHOfField(hOfField);
        setMaxSpeed(maxSpeed);
        setLimitTime(limitTime);
        setMaxLength(maxLength);
        setMaxE(maxE);
    }

    public void saveToFile(String fileName) {
        saveToFile(fileName, false);
    }

    public void saveToFile(String fileName, boolean verbose) {
        try {
            File newFile = new File(fileName);
            newFile.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(this.numOfSensors + "");
            writer.newLine();
            writer.write(this.wOfField + " " + this.hOfField);
            writer.newLine();

            for (int i = 0; i < this.numOfSensors; i++) {
                Sensor sensor = this.listSensors.get(i);
                writer.write(sensor.getX() + " " + sensor.getY() + " " + sensor.getR());
                writer.newLine();
            }

            writer.write(this.start.getX() + " " + this.start.getY());
            writer.newLine();

            writer.write(this.dest.getX() + " " + this.dest.getY());
            writer.newLine();

            writer.write(this.maxSpeed + "");
            writer.newLine();



            writer.write(this.limitTime + "");
            writer.newLine();

            writer.write(this.maxE + "");

            writer.flush();
            writer.close();
            if (verbose) System.out.println("Completely saved!");
        } catch (IOException e) {
            System.out.println("Error When Writing File To Save Sensor Network");
        }
    }


    public void printInfo() {
        System.out.println("===Information of sensor network===");
        System.out.println("Width: " + this.wOfField + "\t Height: " + this.hOfField);
        System.out.println("Number of sensors: " + this.numOfSensors);
        System.out.println("List sensors:");
        for (int i = 0; i < numOfSensors; i++) {
            Sensor sensor = this.listSensors.get(i);
            System.out.printf("\tSensor %3d:  %10f %10f %10f\n", (i + 1), sensor.getX(), sensor.getY(), sensor.getR());
        }
        System.out.println();
        System.out.println("Speed: " + this.maxSpeed);
        System.out.println("Start: " + this.start.x + "  " + this.start.y);
        System.out.println("Dest: " + this.dest.x + "  " + this.dest.y);
        System.out.println("Limit time: " + this.limitTime);
        System.out.println("Max exposure of each location: " + this.maxE);
    }

    public void initialFromFile(String file) {
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(file)));
            String[] line = sc.nextLine().trim().split(" ");

            setWOfField(Float.parseFloat(line[0]));
            setHOfField(Float.parseFloat(line[1]));

            line = sc.nextLine().trim().split(" ");
            setNumOfSensors(Integer.parseInt(line[0]));

            this.listSensors = new ArrayList<>();
            float x, y, r;
            for (int i = 0; i < this.numOfSensors; i++) {
                line = sc.nextLine().trim().split(" ");
                x = Float.parseFloat(line[0]);
                y = Float.parseFloat(line[1]);


                r = Float.parseFloat(line[2]);
                this.listSensors.add(new Sensor(x, y, r));
            }

            line = sc.nextLine().trim().split(" ");
            setMaxSpeed(Float.parseFloat(line[0]));

            line = sc.nextLine().trim().split(" ");
            setStart(new Location(Float.parseFloat(line[0]), Float.parseFloat(line[1])));

            line = sc.nextLine().trim().split(" ");
            setDest(new Location(Float.parseFloat(line[0]), Float.parseFloat(line[1])));

            line = sc.nextLine().trim().split(" ");
            setLimitTime(Float.parseFloat(line[0]));

            line = sc.nextLine().trim().split(" ");
            setMaxE(Float.parseFloat(line[0]));

            sc.close();
        } catch (FileNotFoundException | NumberFormatException e){
            System.out.println("Error When Read File To Setup Sensor Network");
        }
    }

    public static void main(String[] args) throws Exception {
        int[] listNumberSensors = {10,20,50,100,200};
        int numberTestForOnce = 10;
        File fileRoot = new File("input");
        fileRoot.mkdir();

        for (int numberSensors : listNumberSensors) {
            File file = new File("input//" + numberSensors);
            boolean mkdir = file.mkdir();
            if(mkdir) {
                System.out.println("Dictionary created successful");
            }
            else {
                System.out.println("Dictionary created failed");
            }

            for (int j = 0; j < numberTestForOnce; j++) {
                SensorNetwork sNet = new SensorNetwork();

                sNet.randomInitial(100, 100, numberSensors, 7, 5, 100, 500, numberSensors / 2);
                sNet.saveToFile("input//" + numberSensors + "//" + numberSensors + "_" + (j + 1) + ".txt");
                //sNet.initialFromFile("input//" + numberSensors + ".txt");
                sNet.printInfo();
            }
        }
    }
}
