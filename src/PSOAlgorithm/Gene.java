/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PSOAlgorithm;

import model_PSO.Point;
import model_PSO.Objective;

/**
 *
 * @author longy
 */
public class Gene extends Point {
    public double ip;
    public double v;

    public Gene(double x, double y, Objective ob) {
        super(x, y);
        this.ip = ob.getIP(x,y);
        this.v = 0;
    }

    public Gene(double x, double y, double x1, double y1, Objective ob) {
        super(x, y);
        this.ip = ob.getIP(x, y);
        this.v = 0;
    }

}
