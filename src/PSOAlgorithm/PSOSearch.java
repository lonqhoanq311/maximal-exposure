/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PSOAlgorithm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import model_PSO.Objective;
import model_PSO.Pbest;
import model_PSO.Point;
import model_PSO.Sensor;

/**
 *
 * @author longy
 */
public class PSOSearch {
	// So ca the cua quan the
	public static final int POPNUM = 1000;

	// So luong the he
	public static final int PSOINTER = 200;
	
	// can thay doi 3 he so nay ( he so quan tinh w - he so van toc theo maxgene, he so van toc theo doi tuong lon nhat)
	
	public static final double C = 0.3;
	public static final double C1 = 0.5;
	public static final double C2 = 2;

	public static final int SOCATHE = POPNUM;
	// Quan the
	public Individual[] population;

	// cac rang buoc (moi them - dlg)
	
	public static double speed = 5, limitTime = 100, limitS = speed * limitTime;
	public static int N;
	public static double x1 = 0, x2 = 0, y1 = 0, y2 = 0;
	
	public Random rand = new Random();
	public Objective ob;
	public Point[] ketqua;
	public static double maxEP;
	public static double timeC = 0;
	
//	public PSOSearch() {
//		this.Objective.sensors = new ArrayList<Sensor>();
//	}
	
	// khoi tao sensor
	public void init(String inputFile) {
		Objective.sensors = new ArrayList<Sensor>();
		this.ob = new Objective(Objective.sensors, ob.W, ob.H);
		FileReader inFileReader;
		BufferedReader in;
		try {
			inFileReader = new FileReader(inputFile);
			in = new BufferedReader(inFileReader);
			String[] line;
			line = in.readLine().split(" ");
			int numOfSensors = Integer.parseInt(line[0]);
			in.readLine();
			for (int i = 0; i < numOfSensors; i++) {
				line = in.readLine().split(" ");
				float x = Float.parseFloat(line[0]);
				float y = Float.parseFloat(line[1]);
				float r = Float.parseFloat(line[2]);
				Sensor sensorRaw = new Sensor(x,y,r);
				Objective.sensors.add(sensorRaw);
			}

			line = in.readLine().split(" ");
			x1 = Double.parseDouble(line[0]);
			y1 = Double.parseDouble(line[1]);

			line = in.readLine().split(" ");
			x2 = Double.parseDouble(line[0]);
			y2 = Double.parseDouble(line[1]);

			in.close();
		} catch (NumberFormatException | IOException e) {
			System.out.println("Error When Read File To Setup Sensor Network");
		}

	}
	// Khoi tao quan the
	public Individual[] InitSolution() {
		Individual[] ps = new Individual[POPNUM];
		int i = 0;
		int k=0;
		for (k = 0; k < POPNUM; k++) { 
			ps[i++] = new Individual(ob);
//			System.out.println("Mep = " + ps[k].getObjective());
		}
		
		//System.out.println("XONG KHOI TAO QUAN THE");
		return ps;
	}
	
	public void searchPSO(int iter, String inputFile) throws IOException {
		long start = System.currentTimeMillis();
		init(inputFile);
		//System.out.println("Khoi tao quan the ");
		population = InitSolution(); 

		Pbest[] Pbest = new Pbest[SOCATHE];

		for (int i = 0; i < SOCATHE; i++) {
			Pbest[i] = new Pbest();
		}

		Pbest Gbest = new Pbest();

		// Khoi tao Pbest
		for (int i = 0; i < SOCATHE; i++) {
			Pbest[i].point = population[i].Points();
			Pbest[i].Objective = population[i].getObjective();
			
		}

		// Tim Gbest
		// Tim Gbest trong tat ca cac Pbest
		int xacDinhCaTheGbest = 0;
		for (int a = 1; a < SOCATHE; a++) {
//			System.out.println(" Pbest[a].Objective = " + Pbest[a].Objective);
			if (Pbest[xacDinhCaTheGbest].Objective < Pbest[a].Objective) {
				xacDinhCaTheGbest = a;
			}
		}
		
		Gbest = Pbest[xacDinhCaTheGbest];
	
//		int xacDinhPbestToiNhat = 0;
		int theHe = 0;
		ArrayList<Double> vanToc = new ArrayList<>();
		do {
			++theHe;
//			System.out.println("Gbest Mep = " + Gbest.Objective);
			for (int k = 0; k < SOCATHE; k++) {

				double r1 = rand.nextDouble();
				double r2 = rand.nextDouble();

				for (int j = 0; j < population[0].getSize(); j++) {
//					if(j != 0 && j != population[0].getSize() - 1 ) {
//						population[k].getGene(j).v = rand.nextDouble();
//					}
					double x_ti = population[k].getGene(j).x;

					double v_t1i = C * population[k].getGene(j).v + C1 * r1 * (Pbest[k].point[j].x - x_ti) + C2 * r2 * (Gbest.point[j].x - x_ti);
					double x_t1i = x_ti + v_t1i;
					
					if(x_t1i >0 && x_t1i<ob.W) {
						population[k].setGene(j, new Gene(x_t1i, population[k].getGene(j).y, ob));
					}
						
					population[k].genes[j].v = v_t1i;
					
//					if(j == 0) v_t1i = 0;
//					if(j == population[0].getSize() - 1) v_t1i = 0;
					
					vanToc.add(v_t1i);
//					System.out.println("van toc la " + v_t1i);
//					if(j == 0) {
//						System.out.println("van toc la " + v_t1i);
//					}
				}
				
				for (int j = 0; j < population[0].getSize(); j++) {
					population[k].genes[j].v = vanToc.get(j);
				}
				
				vanToc.clear();

				if (Pbest[k].Objective < population[k].getObjective()) { //xac dinh Pbest
					Pbest[k].point = population[k].Points();
					Pbest[k].Objective = population[k].getObjective();
				}
			}

			// Xac dinh lai ca the Gbest
			xacDinhCaTheGbest = 0;
			for (int a = 1; a < SOCATHE; a++) {
				if (Pbest[xacDinhCaTheGbest].Objective < Pbest[a].Objective && a!=POPNUM-1 && a!= POPNUM-2) {
					xacDinhCaTheGbest = a;
				}
			}
			//System.out.println("Ca the best la " + Pbest[xacDinhCaTheGbest].Objective);
			Gbest = Pbest[xacDinhCaTheGbest];

		} while (theHe < PSOINTER && Gbest.Objective > 0);


		this.maxEP = Gbest.Objective;
		timeC = System.currentTimeMillis() - start;
	}
	
	public double[] RunAlgo(String inputFile) throws IOException{
		searchPSO(PSOINTER, inputFile);
		return new double[] {maxEP, timeC};
	}


	public static void main(String[] args) throws IOException  {
		String inputFolder = "./input/";

		int[] listNumberSensors = {10,20,50};
		int numOfTestEachCase = 3;//10;
		System.out.printf("%-20s \t %-10s \t %-10s  \n","FILE","Exposure","RUNTIME");
		for (int numberSensors : listNumberSensors) {
			for (int i = 1; i <= numOfTestEachCase; i++) {
				PSOSearch psoSearch = new PSOSearch();
				String inputFile = inputFolder + numberSensors + "//" + numberSensors + "_" + i + ".txt";
				double[] result = psoSearch.RunAlgo(inputFile);
				System.out.printf("%-20s \t %-10f \t %-10f\n", "data_" + numberSensors + "_" + i + ".txt", result[0], result[1]);
			}
		}
		
	}
}
